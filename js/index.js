var dataList = [{
    "id": 101,
    "name": "Smith Jeff",
    "amount": "4200",
    "closeDate": "11/03/2016"
}, {
    "id": 102,
    "name": "Wislson Ronald",
    "amount": "1300",
    "closeDate": "13/01/2014"
}, {
    "id": 103,
    "name": "Ricky Whales",
    "amount": "5000",
    "closeDate": "21/04/2014"
}, {
    "id": 104,
    "name": "Tony Mahal",
    "amount": "4200",
    "closeDate": "11/03/2016"
}, {
    "id": 105,
    "name": "Greg Lawson",
    "amount": "10000",
    "closeDate": "13/01/2014"
}, {
    "id": 106,
    "name": "Andew Hayden",
    "amount": "34568",
    "closeDate": "21/04/2014"
}, {
    "id": 107,
    "name": "Lendl Simmons",
    "amount": "1700",
    "closeDate": "11/03/2016"
}, {
    "id": 108,
    "name": "Ricky Williams",
    "amount": "14821",
    "closeDate": "13/01/2014"
}, {
    "id": 109,
    "name": "Paul James",
    "amount": "6921",
    "closeDate": "21/04/2014"
}, {
    "id": 110,
    "name": "Manoj Chandrew",
    "amount": "79021",
    "closeDate": "11/03/2016"
}, {
    "id": 111,
    "name": "Lium Plunkett",
    "amount": "583214",
    "closeDate": "13/01/2014"
}, {
    "id": 112,
    "name": "Voila Ledlon",
    "amount": "7347",
    "closeDate": "21/04/2014"
}, {
    "id": 113,
    "name": "Simon Katich",
    "amount": "67842",
    "closeDate": "11/03/2016"
}, {
    "id": 114,
    "name": "Richard Lawson",
    "amount": "5642",
    "closeDate": "13/01/2014"
}, {
    "id": 115,
    "name": "Chris Henry",
    "amount": "2463",
    "closeDate": "21/04/2014"
}, {
    "id": 116,
    "name": "Taufel Buckener",
    "amount": "34577",
    "closeDate": "11/03/2016"
}, {
    "id": 117,
    "name": "Smith White",
    "amount": "13800",
    "closeDate": "13/01/2014"
}, {
    "id": 118,
    "name": "Darren Washington",
    "amount": "9422",
    "closeDate": "21/04/2014"
}, {
    "id": 119,
    "name": "Paul Gayle",
    "amount": "1479",
    "closeDate": "11/03/2016"
}, {
    "id": 120,
    "name": "Kumarathan Philips",
    "amount": "6733",
    "closeDate": "13/01/2014"
}, {
    "id": 121,
    "name": "Wisom Lindon",
    "amount": "9953",
    "closeDate": "21/04/2014"
}, {
    "id": 122,
    "name": "Heshelle Gibyason",
    "amount": "97656",
    "closeDate": "11/03/2016"
}, {
    "id": 123,
    "name": "Rock Bottom",
    "amount": "54634",
    "closeDate": "13/01/2014"
}, {
    "id": 124,
    "name": "Ronald Plukins",
    "amount": "4236",
    "closeDate": "21/04/2014"
}];
$(function() {
    initialize();
    performAction();
});

var initialize = function() {
    for (var i in dataList) {
        var currentData = dataList[i];
        $('.open-opportunities').find('tbody').append(
            '<tr>' +
            '<td>' + currentData.name + '</td>' +
            '<td>$' + currentData.amount + '</td>' +
            '<td>' + currentData.closeDate + '</td>' +
            '</tr>'
        );

        $('.best-interest-search-list-section .list-section').append(
            '<div class="form-group">' + currentData.name + '</div>'
        );

    }
};

//chartjs

// Chart options
Chart.defaults.global.legend.display = false;
Chart.defaults.global.tooltips.enabled = false;

var performAction = function() {
    $('#interest-search').on('keyup', function() {
        $('.list-section').empty();
        var text = $(this).val().toLowerCase();
        if (text.length > 0) {
            var newDataList = [];
            for (var i in dataList) {
                var dataName = dataList[i];
                if (dataName.name.toLowerCase().indexOf(text) >= 0) {
                    newDataList.push(dataName.name);
                }
            }
            if (newDataList.length > 0) {
                for (var i in newDataList) {
                    var dataName = newDataList[i];
                    $('.list-section').append(
                        '<div class="form-group">' + dataName + '</div>'
                    );
                }
            }
        } else {
            initialize();
        }
    });
};

var random = function() {
    return Math.round(Math.random() * 95)
};

var barChartData = {
    labels: ["BIC", "BID", "PTE 774", "Opt out", "Grand Fathered", "INIGO"],
    datasets: [{
        backgroundColor: "rgba(86,192,224,0.5)",
        borderColor: "rgba(0,0,0,0)",
        highlightFill: "rgba(151,187,205,0.8)",
        data: [random(), random(), random(), random(), random(), random()]
    }]
}
var options = {
    responsive: true,
    animation: false,
    showScale: false,
    scaleShowGridLines: false,
		    scaleShowLabels: false,
    barShowStroke: false,
    tooltipXPadding: 10,
    tooltipYPadding: 6,
    tooltipFontSize: 18,
    tooltipFontStyle: 'bold',
    scales: {
        xAxes: [{
            gridLines: {
                display: false
            }
        }],
				yAxes: [{
								display: false
				}]
    }
};
// var ctx = $("#canvas").get(0).getContext("2d");

var ctx = document.getElementById("canvas").getContext("2d");
// window.myBar = new Chart(ctx).Bar(barChartData, opt);
var myBarChart = new Chart(ctx, {
    type: 'bar',
    data: barChartData,
    options: options,
    legend: {
        display: false,
    },
});
